// ****************************************************************************
//  vfdload.c                        (C) 1992-2003 Christophe de Dinechin (ddd) 
//                                                            Activity project 
// ****************************************************************************
// 
//   File Description:
// 
//     Display the current load using a bar graph
// 
// 
// 
// 
// 
// 
// 
// 
// ****************************************************************************
// This document is confidential.
// Do not redistribute without written permission
// ****************************************************************************
// * File       : $RCSFile$
// * Revision   : $Revision$
// * Date       : $Date$
// ****************************************************************************

#define VFDCLIENT               "vfdload"
#define VFDOPTIONS              "vfdload.tbl"
#define VFDCONFIG               "load"
long refresh_rate = 100000;

#include "vfdclient.h"

typedef struct load
// ----------------------------------------------------------------------------
//   Information about system load
// ----------------------------------------------------------------------------
{
    unsigned long       user, sys, nice, idle, total;
} load_t;


int main(int argc, char *argv[])
// ----------------------------------------------------------------------------
//   Main entry point
// ----------------------------------------------------------------------------
{
     int vfd;
     vfd_configuration cf;
     int x, y, w, h;
     float load1, load2, load3;
     int h1, h2, h3;
     int by;
     load_t cur, prev, diff;
     int user[256], sys[256];
     int index = 0;
     int i;

     vfd_options(argc, argv);
     vfd = vfd_open(server, port);

     // Use configuration
     cf = vfd_configure(vfd, cfg);
     x = cf.x; y = cf.y; w = cf.w; h = cf.h;
     by = y + h - 1;

     while (1)
     {
         FILE *f = fopen ("/proc/stat", "r");
         fscanf(f, "cpu %lu %lu %lu %lu",
                &cur.user, &cur.nice, &cur.sys, &cur.idle);
         fclose(f);
         cur.total = cur.user + cur.nice + cur.sys + cur.idle;
         
         diff.user = cur.user - prev.user;
         diff.sys  = cur.sys - prev.sys;
         diff.nice = cur.nice - prev.nice;
         diff.idle = cur.idle - prev.idle;
         diff.total = cur.total - prev.total;
         
         prev = cur;
         
         if (!diff.total)
             continue;
         
         user[index] = (h -2) * (diff.user + diff.nice) / diff.total;
         sys[index] = (h - 2) * diff.sys / diff.total;
         
         vfd_clear(vfd);
         vfd_rectangle(vfd, x, y, w, h);
         for (i = 0; i < w; i++)
         {
             int user_h = user[(index-i) & 255];
             int sys_h = sys[(index - i) & 255];
             vfd_fill_rectangle(vfd,
                                x + w - 1 - i, by - user_h,
                                1, user_h);
             vfd_fill_rectangle(vfd,
                                x + w - 1 - i, y,
                                1, sys_h);
         }
         vfd_update(vfd);
         
         index = (index + 1) & 255;
         usleep(refresh_rate);
     }
     return 0;
}

