// ****************************************************************************
//  vfdclock.c                     (C) 1992-2003 Christophe de Dinechin (ddd) 
//                                                            VFD800 project 
// ****************************************************************************
// 
//   File Description:
// 
//     Analog clock using the VFD800 server
//
// 
// 
// 
// 
// 
// 
// 
// ****************************************************************************
// This document is confidential.
// Do not redistribute without written permission
// ****************************************************************************
// * File       : $RCSFile$
// * Revision   : $Revision$
// * Date       : $Date$
// ****************************************************************************

#include <math.h>
#include <time.h>

#define VFDCLIENT               "vfdclock"
#define VFDOPTIONS              "vfdclock.tbl"
#define VFDCONFIG               "clock"
#include "vfdclient.h"


void RunClock(int vfd, char *config)
// ----------------------------------------------------------------------------
//   Loop, displaying the clock
// ----------------------------------------------------------------------------
{
     char line[256];
     vfd_configuration cfg;
     int x, y, w, h;
     int first = 1;

     // Wait for the configuration to actually display something
     do
     {
          if (!first)
               sleep(1);
          first = 0;
          cfg = vfd_configure(vfd, config);
     } while (cfg.w < 10 || cfg.h < 10);
     x = cfg.x; y = cfg.y; w = cfg.w; h = cfg.h;

     // Set mask to cover the clock itself
     vfd_select_mask(vfd);
     vfd_clear(vfd);
     vfd_fill_ellipse(vfd, x, y, w, h);
     vfd_select_display(vfd);

     // Display inner circle
     w -= 2;
     h -= 2;
     x += 1;
     y += 1;
     vfd_ellipse(vfd, x, y, w, h);

     while (1)
     {
          time_t t = time(NULL);
          struct tm *tm = localtime(&t);

          int x0 = x + w/2;
          int y0 = y + h/2;

          int wsec = w / 2.5 * sin(tm->tm_sec / 60.0 * M_PI * 2);
          int hsec = -h / 2.5 * cos(tm->tm_sec / 60.0 * M_PI * 2);

          int wmin = w / 3.0 * sin(tm->tm_min / 60.0 * M_PI * 2);
          int hmin = -h / 3.0 * cos(tm->tm_min / 60.0 * M_PI * 2);

          int whr = w / 3.5 * sin(tm->tm_hour / 12.0 * M_PI * 2);
          int hhr = -h / 3.5 * cos(tm->tm_hour / 12.0 * M_PI * 2);

          // Draw the hands
          vfd_set_color(vfd, 1);
          vfd_line(vfd, x0, y0, whr, hhr);
          vfd_line(vfd, x0, y0, wmin, hmin);
          vfd_line(vfd, x0, y0, wsec, hsec);
          vfd_update(vfd);

          // Print date, time

          // Sleep
          usleep(1000000);

          // Erase the hands (don't update to avoid blinking)
          vfd_set_color(vfd, 0);
          vfd_line(vfd, x0, y0, whr, hhr);
          vfd_line(vfd, x0, y0, wmin, hmin);
          vfd_line(vfd, x0, y0, wsec, hsec);
     }
}


int main(int argc, char *argv[])
// ----------------------------------------------------------------------------
//   Main entry point
// ----------------------------------------------------------------------------
{
    int vfd;
    vfd_options(argc, argv);
    vfd = vfd_open(server, port);
    RunClock(vfd, cfg);
    return 0;
}

