// ****************************************************************************
//  vfdticker.c                     (C) 1992-2003 Christophe de Dinechin (ddd) 
//                                                            VFD800 project 
// ****************************************************************************
// 
//   File Description:
// 
//    Scroll input text by, continuously
// 
// 
// 
// 
// 
// 
// 
// 
// ****************************************************************************
// This document is confidential.
// Do not redistribute without written permission
// ****************************************************************************
// * File       : $RCSFile$
// * Revision   : $Revision$
// * Date       : $Date$
// ****************************************************************************

#define VFDCLIENT               "vfdticker"
#define VFDOPTIONS              "vfdticker.tbl"
#define VFDCONFIG               "ticker"
long refresh_rate = 20000;

#include "vfdclient.h"


int main(int argc, char *argv[])
// ----------------------------------------------------------------------------
//   Main entry point
// ----------------------------------------------------------------------------
{
     int vfd;
     char line[256];
     char width[256];
     vfd_configuration cf;
     int x, y, w, h;
     int c;
     int cx;
     int len = 0;
     int first_w = 0, last_w = 0, line_w = 0;
     vfd_glyph_extent extent;
 
     vfd_options(argc, argv);
     vfd = vfd_open(server, port);

     // Use configuration
     cf = vfd_configure(vfd, cfg);
     x = cf.x; y = cf.y; w = cf.w; h = cf.h;

     line[len] = 0;
     while (!feof(stdin))
     {
          c = getchar();
          if (c == EOF)
               break;

          // Turn all controls to spaces
          if (c < ' ')
               c = ' ';

          // Compute extent of new character
          extent = vfd_printf_area(vfd, "%c", c);
          last_w = extent.w;
          line_w += last_w;
          width[len] = last_w;

          // Store new character at end of string
          if (len > sizeof(line) - 1)
               exit(2);
          line[len++] = c;
          line[len] = 0;

          // If line is too short for a scroll, get next character
          first_w = width[0];
          if (line_w < w + first_w)
               continue;

          // Scroll from the right
          for (cx = x; cx > x - first_w; cx--)
          {
               vfd_printf(vfd, cx, y, "%s", line);
               vfd_update(vfd);
               usleep(refresh_rate);
          }

          // Move line to the left
          memmove(line, line+1, len);
          memmove(width, width+1, len);
          line_w -= first_w;
          len -= 1;          
     }
    return 0;
}

