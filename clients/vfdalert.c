// ****************************************************************************
//  vfdalert.c                      (C) 1992-2003 Christophe de Dinechin (ddd) 
//                                                            VFD800 project 
// ****************************************************************************
// 
//   File Description:
// 
//     Pops up an alert on the display
// 
// 
// 
// 
// 
// 
// 
// 
// ****************************************************************************
// This document is confidential.
// Do not redistribute without written permission
// ****************************************************************************
// * File       : $RCSFile$
// * Revision   : $Revision$
// * Date       : $Date$
// ****************************************************************************

#define VFDCLIENT               "vfdalert"
#define VFDOPTIONS              "vfdalert.tbl"
#define VFDCONFIG               "alert"

long on_time = 500000;
long off_time = 300000;
long repeats = 3;

#include "vfdclient.h"


void RunAlert(int vfd, char *config)
// ----------------------------------------------------------------------------
//   Loop, displaying the alert message
// ----------------------------------------------------------------------------
{
     char line[256];
     vfd_configuration cfg;
     int x, y, w, h;
     int color = 1;
     vfd_glyph_extent extent;
     long i;

     // Wait for the configuration to actually display something
     cfg = vfd_configure(vfd, config);
     x = cfg.x; y = cfg.y; w = cfg.w; h = cfg.h;

     bzero(line, sizeof(line));
     while (!feof(stdin))
     {
          int cx, cy;

          if (!fgets(line, sizeof(line)-1, stdin))
               break;

          extent = vfd_printf_area(vfd, "%s", line);
          cx = x + (w - extent.w)/2;
          cy = y + (h - extent.h)/2;

          for (i = 0; i < repeats; i++)
          {
               vfd_set_color(vfd, 1);
               vfd_clear(vfd);
               vfd_printf(vfd, cx, cy, "%s", line);
               vfd_update(vfd);
               
               // Sleep
               usleep(on_time);

               vfd_set_color(vfd, 0);
               vfd_clear(vfd);
               vfd_printf(vfd, cx, cy, "%s", line);
               vfd_update(vfd);

               // Sleep
               usleep(off_time);
          }
     }
}


int main(int argc, char *argv[])
// ----------------------------------------------------------------------------
//   Main entry point
// ----------------------------------------------------------------------------
{
    int vfd;
    vfd_options(argc, argv);
    vfd = vfd_open(server, port);
    RunAlert(vfd, cfg);
    return 0;
}

