#ifndef VFDSERVER_H
#define VFDSERVER_H
// ****************************************************************************
//  vfdserver.h                     (C) 1992-2003 Christophe de Dinechin (ddd) 
//                                                            VFD800 project 
// ****************************************************************************
// 
//   File Description:
// 
//     Data structures used by the VFD server
// 
// 
// 
// 
// 
// 
// 
// 
// ****************************************************************************
// This document is confidential.
// Do not redistribute without written permission
// ****************************************************************************
// * File       : $RCSFile$
// * Revision   : $Revision$
// * Date       : $Date$
// ****************************************************************************

typedef unsigned char byte;

#define VFD_COLS        160
#define VFD_LINES       4
#define VFD_SIZE        (VFD_COLS * VFD_LINES)
#define VFD_PLINES      (VFD_LINES * 8)


typedef struct vfdclient *vfdclient_p;
typedef struct vfdclient
// ----------------------------------------------------------------------------
//   Information about a client of this server
// ----------------------------------------------------------------------------
{
     vfdclient_p        next;

     // Display contents
     byte               valid[VFD_SIZE];        // The committed bytes to display
     byte               display[VFD_SIZE];      // The bytes to display
     byte               shape[VFD_SIZE];        // The client requested shape
     byte               mask[VFD_SIZE];         // Compositing mask
     byte *             base;                   // Where we are writing
     volatile int       reader_index;           // Updated by display refresh
     volatile int       writer_index;           // Updated by content task
     char *             glyphs[256];            // Glyphs for that client
     int                priority;               // High priority in foreground

     // Command processing
     int                socket;
     char               command[256];
     char *             head;
     char *             tail;
     char               name[32];
} vfdclient_t;

#endif /* VFDSERVER_H */
