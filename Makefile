#******************************************************************************
# Makefile                          (C) 1992-2003 Christophe de Dinechin (ddd) 
#                                                             VFD800 project 
#******************************************************************************
#
#  File Description:
#
#    Top level makefile for the VFD server
#
#
#
#
#
#
#
#
#******************************************************************************
#This document is confidential.
#Do not redistribute without written permission
#******************************************************************************
#* File       : $RCSFile$
#* Revision   : $Revision$
#* Date       : $Date$
#******************************************************************************

SUBDIRS=test showkeys server clients

include $(BROOT)Makefile.config

PREFIX=/usr/local
INSTALL=/usr/bin/install

SERVER_BIN=	vfdserver
CLIENT_BIN=	vfdbrightness 			\
		vfdload vfdclock 		\
		vfddisplay vfdticker vfdalert	\
		vfdslashdot vfdweather

install: 	$(SERVER_BIN:%=%.sbin-install) 	\
		$(CLIENT_BIN:%=%.bin-install)	\
	$(INSTALL) etc/vfd.conf /etc/vfd.conf
	$(INSTALL) etc/init.d/de200vfd /etc/init.d/de200vfd

%.sbin-install:
	$(INSTALL) -c server/$* $(PREFIX)/sbin/$*
%.bin-install:
	$(INSTALL) -c clients/$* $(PREFIX)/bin/$*
